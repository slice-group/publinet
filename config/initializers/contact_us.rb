# Agregar datos de configuración
KepplerContactUs.setup do |config|
	config.mailer_to = "info@publinetecuador.com"
	config.mailer_from = "info@publinetecuador.com"
	config.name_web = "Publinet"
	#Route redirection after send
	config.redirection = "/"


	# Agregar keys de google recaptcha
	Recaptcha.configure do |config|
	  config.public_key  = "6LcwXhATAAAAAM3awPe6Rg89nCvv4w5xkvwHmc86"
	  config.private_key = "6LcwXhATAAAAABj_-rRxmv8lds7Q0No2xZr3azJS"
	end
end