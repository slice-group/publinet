Rails.application.routes.draw do

  mount Ckeditor::Engine => '/ckeditor'
  root to: 'frontend#index'
  get '/sobre-nosotros', to: 'frontend#about', as: 'about'
  get '/servicios/:permalink', to:'frontend#service', as: 'service_description'
  get '/trabaja-con-nosotros', to: 'frontend#contact', as: 'contact'

  devise_for :users, skip: KepplerConfiguration.skip_module_devise

  mount KepplerContactUs::Engine, :at => '/', as: 'messages'
  mount KepplerAdwords::Engine, at: '/', as: 'adwords'

  resources :admin, only: :index

  scope :admin do

    resources :users do
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end
    resources :clients do
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end
    resources :services do
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

  end

  #errors
  match '/403', to: 'errors#not_authorized', via: :all, as: :not_authorized
  match '/404', to: 'errors#not_found', via: :all
  match '/422', to: 'errors#unprocessable', via: :all
  match '/500', to: 'errors#internal_server_error', via: :all


  #dashboard
  mount KepplerGaDashboard::Engine, :at => '/', as: 'dashboard'

  #Routing Blog

  #get '(*path)' => 'application#blog', :constraints => { :subdomain => 'http://blog.publinetecuador.com' }

  get '/blog' => redirect("/blog/")

end
