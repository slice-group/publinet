# This migration comes from keppler_contact_us (originally 20150714180631)
class CreateKepplerContactUsMessages < ActiveRecord::Migration
  def change
    create_table :keppler_contact_us_messages do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :company
      t.string :city
      t.string :web
      t.string :subject
      t.text :content
      t.boolean :read

      t.timestamps null: false
    end
  end
end
