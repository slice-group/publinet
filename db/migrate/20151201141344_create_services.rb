class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :name
      t.string :permalink
      t.string :short_description
      t.text :large_description
      t.string :category

      t.timestamps null: false
    end
  end
end
