# This migration comes from keppler_adwords (originally 20151123135340)
class RemoveDescriptionViewAndTitleViewFromAdwords < ActiveRecord::Migration

  def up
    remove_column :keppler_adwords_adwords, :title_view
    remove_column :keppler_adwords_adwords, :description_view
  end

  def down
    add_column :keppler_adwords_adwords, :title_view, :string
    add_column :keppler_adwords_adwords, :description_view, :string
  end

end
