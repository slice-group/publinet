class RemoveSubjectFromKepplerContactUsMessages < ActiveRecord::Migration
  def change
    remove_column :keppler_contact_us_messages, :subject, :string
  end
end
