class FrontendController < ApplicationController
	layout 'layouts/frontend/application'
	
  def index
  	@message = KepplerContactUs::Message.new
  	@advisors = Service.where category: "Advisor"
  	@sociales = Service.where category: "Social"
  	@creativos = Service.where category: "Creativo"
  end

  def about	
  	@message = KepplerContactUs::Message.new
  	@clients = Client.all
  end
  def service
  	@service = Service.find_by permalink: params[:permalink]
    @message = KepplerContactUs::Message.new
  end
  def contact
    @message = KepplerContactUs::Message.new
  end

end
