#Generado por keppler
require 'elasticsearch/model'
class Service < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  before_save :create_permalink
  validates_presence_of :name, :large_description, :short_description, :category
  validates_uniqueness_of :name
  validate :validates_category
  
  after_commit on: [:update] do
    puts __elasticsearch__.index_document
  end
  
  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [:id, :name, :short_description, :large_description, :category] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      name:  self.name,
      short_description:  self.short_description.to_s,
      large_description:  self.large_description.to_s,
      category:  self.category.to_s
    }.as_json
  end

  def validates_category
    errors.add(:errors, "Debe seleccionar una categoría") unless (self.category == "Advisor") || (self.category == "Social") || (self.category == "Creativo")  
  end

  private

  def create_permalink
    self.permalink = self.name.downcase.parameterize
  end

end
#Service.import
