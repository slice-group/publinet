function removeParams() { 
	history.pushState("", document.title, window.location.href.split('#')[0]);
}
var c_time = 0;
var event = 'onwheel' in document ? 'wheel' : 'onmousewheel' in document ? 'mousewheel' : 'DOMMouseScroll';
window.addEventListener(event, callback);

function callback(event) {
	removeParams();
	var normalized;
	if (event.wheelDelta) {
		normalized = (event.wheelDelta % 120 - 0) == -0 ? event.wheelDelta / 120 : event.wheelDelta / 12;
	} else {
		var rawAmmount = event.deltaY ? event.deltaY : event.detail;
		normalized = -(rawAmmount % 3 ? rawAmmount * 10 : rawAmmount / 3);
	}

	if(($.now() - c_time) > 500){
		if ($(window).width() >= 992 ){
			if($('.landing').hasClass('current')){
				if(normalized == -1){
					if($('.active').attr('id') != 'section-4'){ scrollDown(); }
				}else{
					if($('.active').attr('id') != 'section-home'){ scrollUp(); }
				}
			}
			else if($('.about').hasClass('current')){
				if(normalized == -1){
					if($('.active').attr('id') != 'section-4'){ scrollDownAbout(); }
				}else{
					if($('.active').attr('id') != 'section-2'){ scrollUpAbout(); }
				}
			}
			else if($('.services').hasClass('current')){
				if(normalized == -1){
					if($('.img').hasClass('active')){ 
						if( ($('.text').css('overflow') != 'visible') && $('.text').scrollTop() + $('.text').innerHeight()>=$('.text')[0].scrollHeight || $('.img').is(':hover') ){ scrollDownService() }
					}
				}else{
					if($('.footer').hasClass('active')){ scrollUpService(); }
				}
			}
		}
		c_time = $.now();
	}
}

$( document ).ready(function() {
	if (window.location.hash == '#advisor') {
		scrollDown()
	}
	else if (window.location.hash == '#social') {
		scrollDown();
		scrollDown();
	}
	else if (window.location.hash == '#creativo') {
		scrollDown();
		scrollDown();
		scrollDown();
	}
	$('.clients-carrousel').width($('main').width()+'px')

	$('#arrow-back').click(function(){
		if(($.now() - c_time) > 500){
		
			if($('.img-title').text().trim() == "ADVISOR"){
				window.location.href = "/#advisor";
			}
			else if ($('.img-title').text().trim() == "SOCIAL"){
				window.location.href = "/#social";
			}
			else if ($('.img-title').text().trim() == "CREATIVO"){
				window.location.href = "/#creativo";
			}
			c_time = $.now();
		}	
	})

	$('#nav').click(function(){
		if(($.now() - c_time) > 500){
			
			if($('.landing').hasClass('current')){
				if($('#section-home').hasClass('active')){
					scrollDown()
				}
			}
			else{
				window.location.href = "/#advisor";
			}
			c_time = $.now();
		}
	});

	$('#arrow-down').click(function(){
		if(($.now() - c_time) > 500){
			if ($(window).width() >= 992 ){
				if($('.landing').hasClass('current')){
					scrollDown();
				}
				else if($('.about').hasClass('current')){
					scrollDownAbout();
				}
				else if($('.services').hasClass('current')){
					scrollDownService();
				}
			}
			c_time = $.now();
		}
	});
	$('#arrow-up').click(function(){
		if(($.now() - c_time) > 500){
			if ($(window).width() >= 992 ){
				if($('.landing').hasClass('current')){
					scrollUp();
				}
				else if($('.about').hasClass('current')){
					scrollUpAbout();
				}
				else if($('.services').hasClass('current')){
					scrollUpService();
				}
			}
			c_time = $.now();
		}
	});

	$(document).keydown(function(e) {
		switch(e.which) {
				case 38:
					if(($.now() - c_time) > 500){
						if($('.landing').hasClass('current')){
							scrollUp();
						}
						else if($('.about').hasClass('current')){
							scrollUpAbout();
						}
						else if($('.services').hasClass('current')){
							scrollUpService();
						}
						c_time = $.now();
					}
					break;

				case 40:
					if(($.now() - c_time) > 500){
						if($('.landing').hasClass('current')){
							scrollDown();
						}
						else if($('.about').hasClass('current')){
							scrollDownAbout();
						}
						else if($('.services').hasClass('current')){
							scrollDownService();
						}
						c_time = $.now();
					}
					break;

				default: return;
		}
		e.preventDefault();
	});
});


function scrollDown(){
	var act = $('.active').attr('id')
	$('.bounce').removeClass('bounce')

	if(act == "section-home") {
		$('.active').removeClass('active');
		$('#section-1').addClass('active');
		$('.img-1').animate( {top: 0});
		$('.text-1').animate( {top: 0});
		$('.img-1').css('opacity','1');
		$('.text-1').css('opacity','1');
		$('.arrow-up').animate( {top: 0});
		$('#arrow-down').removeClass('infinite');

		$('#arrow-down').addClass('bounce');
		$('#arrow-up').addClass('bounce');

	}
	else if((act == "section-1") || (act == "section-2") ){
		var next = Number(act.replace('section-', ''))+1;

		$('.active').removeClass('active');
		$('#section-'+next).addClass('active');
		$('.img-'+next).animate( {top: 0});
		$('.text-'+next).animate( {top: 0});
		$('.img-'+next).css('opacity','1');
		$('.text-'+next).css('opacity','1');

		$('#arrow-down').addClass('bounce');
		$('#arrow-up').addClass('bounce');
	}
	else if(act == "section-3"){
		$('.active').removeClass('active');
		$('#section-4').addClass('active');

		$('.landing').children().not('.footer').not('.arrow-up').animate({ top: '-350px' });
		$('.footer').animate( {bottom: 0 });

		$('#arrow-up').addClass('bounce');
	}
}

function scrollUp(){
	var act = $('.active').attr('id');
	$('.bounce').removeClass('bounce')

	if(act == "section-1") {
		$('.active').removeClass('active');
		$('#section-home').addClass('active');
		$('#arrow-down').addClass('infinite');

		$('#arrow-down').addClass('bounce');
		$('#arrow-up').addClass('bounce');

		$('.img-1').animate( {top: '100%'}).promise().done(function() {$('.img-1').css({opacity:'0'})});
		$('.text-1').animate( {top: '-100%'}).promise().done(function() {$('.text-1').css({opacity:'0'})});

		$('.arrow-up').animate( {top: '-60px'});
	}
	else if((act == "section-2") || (act == "section-3") ){
		var next = act.replace('section-', '');
		var active = Number(act.replace('section-', ''))-1;
		$('.active').removeClass('active');
		$('#section-'+active).addClass('active');

		if ($('.img-'+next).hasClass('left') == true) {
			$('.img-'+next).animate( {top: '100%'}).promise().done(function() {$('.img-'+next).css({opacity:'0'})});
			$('.text-'+next).animate( {top: '-100%'}).promise().done(function() {$('.text-'+next).css({opacity:'0'})});
		}
		else{
			$('.img-'+next).animate( {top: '-100%'}).promise().done(function() {$('.img-'+next).css({opacity:'0'})});
			$('.text-'+next).animate( {top: '100%'}).promise().done(function() {$('.text-'+next).css({opacity:'0'})});
		}

		$('#arrow-down').addClass('bounce');
		$('#arrow-up').addClass('bounce');

	}
	else if(act == "section-4"){
		$('.active').removeClass('active');
		$('#section-3').addClass('active');

		$('.landing').children().not('.footer').animate({ top: 0 });
		$('.footer').animate( {bottom: '-350px' });

		$('#arrow-down').addClass('bounce');
		$('#arrow-up').addClass('bounce');
	}
}

function scrollDownAbout(){
	var act = $('.active').attr('id')
	$('.bounce').removeClass('bounce')

	if(act == "section-2") {
		$('.active').removeClass('active');
		$('#section-3').addClass('active');
		$('.img-2').animate( {top: 0});
		$('.about-2').animate( {top: 0});
		$('.img-2').css('opacity','1');
		$('.about-2').css('opacity','1');
		$('.arrow-up').animate( {top: 0});
		$('#arrow-down').removeClass('infinite');


		$('#arrow-up').addClass('bounce');
		$('#arrow-down').addClass('bounce');
	}
	else if(act == "section-3"){
		$('.active').removeClass('active');
		$('#section-4').addClass('active');

		$('.about').children().not('.footer').not('.arrow-up').animate({ top: '-350px' });
		$('.footer').animate( {bottom: 0 });

		$('#arrow-up').addClass('bounce');
		$('#arrow-down').addClass('bounce');
	}
}

function scrollUpAbout(){
	var act = $('.active').attr('id');
	$('.bounce').removeClass('bounce')

	if(act == "section-3"){
		var next = act.replace('section-', '');
		var active = Number(act.replace('section-', ''))-1;
		$('.active').removeClass('active');
		$('#section-2').addClass('active');
		$('.arrow-up').animate( {top: '-60px'});
		$('#arrow-down').addClass('infinite');

		$('.img-2').animate( {top: '-100%'}).promise().done(function() {$('.img-2').css({opacity:'0'})});
		$('.about-2').animate( {top: '100%'}).promise().done(function() {$('.about-2').css({opacity:'0'})});

		$('#arrow-up').addClass('bounce');
		$('#arrow-down').addClass('bounce');

	}
	else if(act == "section-4"){
		$('.active').removeClass('active');
		$('#section-3').addClass('active');

		$('.about').children().not('.footer').animate({ top: 0 });
		$('.footer').animate( {bottom: '-350px' });

		$('#arrow-up').addClass('bounce');
		$('#arrow-down').addClass('bounce');
	}
}

function scrollDownService(){
	$('.bounce').removeClass('bounce')
	
	if($('.img').hasClass('active')){
		$('.active').removeClass('active');
		$('.footer').addClass('active');
		$('.arrow-up').animate( {top: 0});
		$('#arrow-up').addClass('bounce');

		$('.services').children().not('.footer').not('.arrow-up').animate({ top: '-350px' });
		$('.footer').animate( {bottom: 0 });

		$('#arrow-up').addClass('bounce');
		$('#arrow-down').addClass('bounce');
	}
}

function scrollUpService(){
	$('.bounce').removeClass('bounce')

	if($('.footer').hasClass('active')){
		$('.active').removeClass('active');
		$('.img').addClass('active');
		$('#arrow-down').addClass('infinite');

		$('.services').children().not('.footer').animate({ top: 0 });
		$('.arrow-up').animate( {top: '-60px'});
		$('.footer').animate( {bottom: '-350px' });
		
		$('#arrow-up').addClass('bounce');
		$('#arrow-down').addClass('bounce');
	}
}


window.setWidth = function setWidth(){
	
	if ($(window).width() < 992 ){
		$('.clients-carrousel').width(Number($('main').width())-20+'px')
		$('.home').height(Number($(window).height())-Number($(window).height()/3)+'px')
		$('#socials').find('.center-h').height('100px')
	}
	else if ($(window).width() >= 992 ){
		$('.clients-carrousel').width(Number($('.about-2').width())-100+'px')
		$('.home').height($(window).height()+'px')
		$('#socials').find('.center-h').height($('#contacts').height()+'px')
	}
}
$(window).resize(function(e) {
  window.setWidth();
});
$(window).load(function(e) {
  $(window).trigger('resize');
  window.setWidth();
});
