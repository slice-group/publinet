var map;
var markersArray = [];
var latlng = new google.maps.LatLng(-0.2870139,-78.4640379);

function initialize()
{  
  var myOptions = {
      zoom: 16,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      scrollwheel: false
  };

  map = new google.maps.Map(document.getElementById("map"), myOptions);
  placeMarker(latlng, map, "Cesar Endara E8-55. Quito, Ecuador");
}

function placeMarker(location, map, title) {    
	var marker = new google.maps.Marker({
	    position: location, 
	    map: map,
	    animation: google.maps.Animation.BOUNCE,
	    title: title,
	    icon: "/assets/frontend/favicon.png"
	});
	// add marker in markers array
	markersArray.push(marker);	
}


google.maps.event.addDomListener(window, 'load', initialize);