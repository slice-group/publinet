#= require jquery
#= require jquery.turbolinks
#= require jquery_ujs
#= require ckeditor/init
#= require angular
#= require sidebar
#= require noty
#= require checklist-model
#= require angular-local-storage
#= require materialize-sprockets
#= require nprogress
#= require nprogress-turbolinks
#= require turbolinks
#= require_tree .

